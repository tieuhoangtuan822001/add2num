import java.util.Scanner;

public class Summation {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Nhap so tu nhien thu nhat");
		String stn1 = scanner.next();
		
		System.out.println("Nhap so tu nhien thu hai");
		String stn2 = scanner.next();
		
		scanner.close();
		
		String total = MyBigNumber.sum(stn1, stn2);
		System.out.println("Ket qua la: "+ total);
		
	}

}
