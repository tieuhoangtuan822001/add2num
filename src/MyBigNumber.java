import java.util.logging.Level;
import java.util.logging.Logger;

public class MyBigNumber
{
	
	synchronized public static String sum(String stn1, String stn2) {
		Logger logger = Logger.getLogger("My log");
		int stn1Length = stn1.length()-1;
		int stn2Length = stn2.length()-1;
		int carry = 0;
		StringBuilder result = new StringBuilder();
		while(stn1Length >= 0 || stn2Length >= 0) {
			
			int stn1Unit = 0;
			int stn2Unit = 0;
			
			if(stn1Length >= 0) {
				stn1Unit = stn1.charAt(stn1Length)-'0';
				stn1Length --;
			}
			
			if(stn2Length >= 0) {
				stn2Unit = stn2.charAt(stn2Length)-'0';
				stn2Length --;
			}
			
			
			int total = stn1Unit + stn2Unit + carry;
			String carryLog = (carry==0) ? "": " Cong tiep voi nho 1 duoc "+ total ;
			int digit = total%10;		
			carry = total/10;
			String totalLog = (total >=10)? "\nLuu " + digit + " vao ket qua va nho 1": "";
			logger.log(Level.INFO, "Lay " + stn1Unit + " cong voi " + stn2Unit + " duoc " + (stn1Unit + stn2Unit)+ "." + carryLog + totalLog);
			
			result = result.append((char)(digit+'0'));
			
		}
		return result.reverse().toString();
	}
}
